package com.example.sweefttask3firstapp

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Parcelable
import android.widget.Toast
import androidx.appcompat.app.AppCompatDelegate
import com.example.sweefttask3firstapp.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding : ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        initListeners()
    }

    private fun initListeners() {
        with(binding) {
            submit.setOnClickListener {
                val username = username.text.toString()
                val age = age.text.toString()
                if (username != EMPTY_STRING && age != EMPTY_STRING)
                    initIntentAndPassParameters(username, age)
                else
                    showToast(MESSAGE)
            }
        }
    }

    private fun initIntentAndPassParameters(username: String, age: String) {
        val sendIntent: Intent = Intent().apply {
            action = Intent.ACTION_SEND
            putExtra(USERNAME, username)
            putExtra(AGE, age)
            type = TYPE_TEXT_
        }

        val shareIntent = Intent.createChooser(sendIntent, null)
        startActivity(shareIntent)
    }

    private fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    companion object {
        const val USERNAME ="username"
        const val AGE = "age"
        const val MESSAGE = "Please fill all fields"
        const val TYPE_TEXT_ = "text/plain"
        const val EMPTY_STRING = ""
    }
}